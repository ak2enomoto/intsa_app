class PasswordResetsController < ApplicationController
  before_action :get_user,   only: [:edit, :update]
  
  def edit
    @user=User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if params[:user][:password].empty?                 
      @user.errors.add(:password, :blank)
      render 'edit'
    elsif @user.update_attributes(user_params)          
      log_in @user
      flash[:success] = "Password has been reset."
      redirect_to @user
    else
      render 'edit'                                    
    end
  end
  
  private
  
    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end

    def get_user
      @user = User.find_by(email: params[:email])
    end

end