class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.text :content
      t.integer :micropost_id
      t.integer :user_id

      t.timestamps
      t.index :user_id
      t.index :micropost_id
  end
    
  end
end
