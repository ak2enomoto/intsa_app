Rails.application.routes.draw do

  root 'static_pages#home'
  get  '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get  '/about',     to: 'static_pages#about'
  
  resources :users
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :microposts,          only: [:new, :index, :show, :create, :destroy]
  resources :microposts do
    resources :comments,          only: [:create, :destroy]
  end
  resources :relationships,       only: [:create, :destroy]
  resources :likes,               only: [:create, :destroy]
  resources :notifications,       only: :index
  resources :password_resets,     only: [:edit, :update]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
